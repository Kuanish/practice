package com.boraji.tutorial.spring.ui.user;

import java.util.Collection;
import java.util.List;

import com.boraji.tutorial.spring.entity.User;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.ValueProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.ItemClick;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.components.grid.ItemClickListener;
import com.vaadin.ui.themes.ValoTheme;

public class UserViewImpl extends VerticalLayout implements UserView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final VerticalLayout root = new VerticalLayout();
	final HorizontalLayout view = new HorizontalLayout();

	private Binder<User> binder = new Binder<>(User.class);

	TextField firstName = new TextField("name");
	TextField lastName = new TextField("surname");
	TextField email = new TextField("email");
	Button save = new Button("Save");
	Button create = new Button("new");
	
	//search elements
	TextField searchByName = new TextField("Search by Name");
	TextField searchBySurname = new TextField("search by surname");
	TextField searchByEmail = new TextField("search by email");
	Button search = new Button("search");

	Grid<User> users = new Grid<User>();
	
	
	//one user for binding
	User bean;
	boolean newUser = true;
	//user presenter
	private UserPresenter presenter;

	public UserViewImpl() {
		
		this.presenter = new UserPresenter(this);
		bean = new User();
		binder.setBean(bean);
		//setting textfields onclicked
		users.addItemClickListener(new ItemClickListener<User>() {
			public void itemClick(ItemClick<User> event) {
				firstName.setValue(event.getItem().getFirstName());
				lastName.setValue(event.getItem().getLastName());
				email.setValue(event.getItem().getEmail());
			}
		});
		
		buildUI();
		bindUI();
		gridUI();
	}

	private void buildUI() {
		// TODO Auto-generated method stub
		
		save.addClickListener(event->{
			if(newUser) {
				try {
					binder.writeBean(bean);
				} catch (ValidationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				presenter.create(bean);
				refreshGrid(users);
			}else {
				test();
			}
		});
		create.addClickListener(event->create());
		
		//search buttons
		search.addClickListener(event->searchName(searchByName.getValue(),
				searchBySurname.getValue(),
				searchByEmail.getValue()));
		
		setMargin(true);
		addComponents(firstName,lastName,email,save,create,view);
		//search components
		view.addComponents(searchByName,searchBySurname,searchByEmail,search);
	}
	
	private void gridUI() {
		// TODO Auto-generated method stub
		
		//creating button listener for deleting the grid elements
		Button.ClickListener myClickListener = new Button.ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				
			}
		};
		List<User> list = presenter.listUsers();
		users.setWidth("650px");
		users.addColumn(user -> user.getId()).setCaption("ID");
		users.addColumn(User::getFirstName).setCaption("Name");
		users.addColumn(User::getLastName).setCaption("LastName");
		users.addColumn(User::getEmail).setCaption("EMAIL");
		//users.addComponentColumn(user -> new Button("delete")).setCaption("delete");
		//users.addComponentColumn(this::buildDeleteButton);
		users.addComponentColumn(this::HL);
		users.setItems((Collection<User>) list);
		addComponent(users);
		
	}
	
	private HorizontalLayout HL(User p) {
		HorizontalLayout HL = new HorizontalLayout();
		HL.addComponent(buildDeleteButton(p));
		HL.addComponent(buildEditButton(p));
		return HL;
	}
	
	private Button buildEditButton(User p) {
		Button button = new Button(VaadinIcons.EDIT);
		button.addStyleName(ValoTheme.BUTTON_SMALL);
		button.addClickListener(e->editUser(p));
		return button;
	}
	
	private void editUser(User p) {
		// TODO Auto-generated method stub
			binder.setBean(p);
			bean = p;
			newUser = false;
	}

	private Button buildDeleteButton(User p) {
		Button button = new Button(VaadinIcons.CLOSE);
		button.addStyleName(ValoTheme.BUTTON_SMALL);
		button.addClickListener(e->deleteUser(p));
		return button;
	}

	private void deleteUser(User p) {
		// TODO Auto-generated method stub
		presenter.delete(p);
		refreshGrid(users);
	}

	private void bindUI() {
		// TODO Auto-generated method stub
		this.binder.forField(firstName).asRequired().bind("firstName");
		this.binder.forField(lastName).bind(User::getLastName, User::setLastName);
		this.binder.forField(email).bind(User::getEmail, User::setEmail);
	}

	private void searchName(String name,String surname, String email) {
		// TODO Auto-generated method stub
		List<User> list = presenter.listUsers();
		list = presenter.search(name,surname,email);
		users.setItems(list);
	}

	private void create() {
		newUser = true;
		binder.setBean(null);
		bean = new User();
	}

	@Override
	public void test() {
			newUser = false;
			presenter.update(bean);
			refreshGrid(users);
		
	}
	
	@Override
	public void update(User bean2) throws ValidationException {
		// TODO Auto-generated method stub
		presenter.update(bean2);
		refreshGrid(users);
	}
	
	@Override
	public void delete() {
		// TODO Auto-generated method stub
		presenter.delete(users.asSingleSelect().getValue());
		refreshGrid(users);
	}
	
	@Override
	public User getModel() {
		// TODO Auto-generated method stub
		return bean;
	}
	
	private void refreshGrid(Grid<User> users) {
		List<User> people = presenter.listUsers();
		users.setItems(people);
	}


	@Override
	public Button deleteButton() {
		// TODO Auto-generated method stub
		return null;
	}
}
