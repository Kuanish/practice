package com.boraji.tutorial.spring.entity;

import lombok.Data;

@Data
public class Person {

	private String name;
	private String surname;
	private int age;
}