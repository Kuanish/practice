package com.boraji.tutorial.spring.ui.user;

import com.boraji.tutorial.spring.entity.User;
import com.vaadin.data.ValidationException;
import com.vaadin.navigator.View;
import com.vaadin.ui.Button;

public interface UserView extends View{
	public void delete();
	public User getModel();
	public Button deleteButton();
	void update(User bean2) throws ValidationException;
	void test();
}
