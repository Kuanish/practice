package com.boraji.tutorial.spring.dao;

import java.util.List;

import com.boraji.tutorial.spring.entity.User;

public interface UserDao {
   void add(User user);
   List<User> listUsers();
boolean delete(Object value);
List<User> getAll();
List<User> search(String name, String surname, String email);
void update(User entity);
}
