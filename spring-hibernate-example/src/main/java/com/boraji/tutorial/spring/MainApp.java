package com.boraji.tutorial.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author imssbora
 *
 */
@SpringBootApplication
public class MainApp {
   public static void main(String[] args){
       SpringApplication.run(MainApp.class, args);

   }
}
