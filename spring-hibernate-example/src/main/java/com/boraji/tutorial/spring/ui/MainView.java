package com.boraji.tutorial.spring.ui;

import com.vaadin.navigator.View;
import com.vaadin.ui.Button;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class MainView extends VerticalLayout implements View {

	public MainView() {
		setSizeFull();
		Button btnUser = new Button("users");
		addComponents(btnUser);
		btnUser.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				doBtnUserClick();
			}

			
			
		});
	}
	private void doBtnUserClick() {
		// TODO Auto-generated method stub
		UI.getCurrent().getNavigator().navigateTo("user");
	}
}
