package com.boraji.tutorial.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.boraji.tutorial.spring.ui.MainView;
import com.boraji.tutorial.spring.ui.user.UserViewImpl;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@SpringUI(path = MainUI.APP_ROOT)
public class MainUI extends UI {
	static final String APP_ROOT = "/vaadin-spring-demo";

	private Navigator navigator;
	
	@Override
	protected void init(VaadinRequest vaadinRequest) {
		final long serialVersionUID = 1L;
		navigator = new Navigator(this,this);
		navigator.addView("", new MainView());
		navigator.addView("user", (Class<? extends View>) UserViewImpl.class);
	}
	
	public Navigator getNavigator() {
		return navigator;
	}
	/*@Autowired
	public UserService service;

	protected void init(VaadinRequest vaadinRequest) {
		final VerticalLayout root = new VerticalLayout();
		final HorizontalLayout view = new HorizontalLayout();

		TextField name = new TextField();
		TextField surname = new TextField();
		TextField email = new TextField();
		Button save = new Button("Save");
		Button delete = new Button("Delete");

		Grid<User> users = new Grid<User>();
		users.addColumn(User::getId).setCaption("ID");
		users.addColumn(User::getFirstName).setCaption("NAME");
		users.addColumn(User::getLastName).setCaption("SURNAME");
		users.addColumn(User::getEmail).setCaption("MAIL");
		refreshGrid(users);

		delete.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				deletePerson();
			}

			public void deletePerson() {
				service.delete(users.asSingleSelect().getValue());
				refreshGrid(users);
			}
		});

		save.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				try {
					updatePerson();
					users.getDataProvider().refreshAll();
				} catch (Exception e) {
					if (!name.isEmpty() && !surname.isEmpty() && !email.isEmpty()) {
						createPerson();
						users.getDataProvider().refreshAll();
					} else {
						Notification.show("Some of the fields are empty");
					}
				}
			}

			private void updatePerson() {
				User clickedUser = users.asSingleSelect().getValue();
				clickedUser.setFirstName(name.getValue());
				clickedUser.setLastName(surname.getValue());
				clickedUser.setEmail(email.getValue());
			}

			private void createPerson() {
				User person = new User();
				person.setEmail(email.getValue());
				person.setFirstName(name.getValue());
				person.setLastName(surname.getValue());
				service.add(person);

				List<User> people = service.listUsers();
				users.setItems(people);
			}
		});

		users.addItemClickListener(new ItemClickListener<User>() {
			@Override
			public void itemClick(ItemClick<User> event) {
				name.setValue(event.getItem().getFirstName());
				surname.setValue(event.getItem().getLastName());
				email.setValue(event.getItem().getEmail());
			}
		});

		root.addComponent(name);
		root.addComponent(surname);
		root.addComponent(email);
		root.addComponent(save);
		root.addComponent(delete);
		view.addComponent(root);
		view.addComponent(users);
		setContent(view);
	}

	private void refreshGrid(Grid<User> users) {
		List<User> people = service.listUsers();
		users.setItems(people);
	}*/

}