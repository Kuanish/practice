package com.boraji.tutorial.spring.service;

import java.util.List;

import com.boraji.tutorial.spring.entity.User;

public interface UserService {
    void add(User user);
	boolean delete(Object object);
    List<User> listUsers();
	List<User> getAll();
	List<User> search(String name, String surname, String email);
	void update(User user);
}
