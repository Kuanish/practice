package com.boraji.tutorial.spring.ui.base;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service
public class MvpInjector implements ApplicationContextAware {
	
    public static ApplicationContext context;

    public static void Inject(final Object object) {
        if (context == null) {
            throw new RuntimeException("MvpInjector => Spring ApplicationContext not init yet or MvpInjector.context is null");
        }
        final AutowireCapableBeanFactory beanFactory = context.getAutowireCapableBeanFactory();
        beanFactory.autowireBean(object);
    }

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext;
	}
}
