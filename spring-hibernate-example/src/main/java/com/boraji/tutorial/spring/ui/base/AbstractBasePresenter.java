package com.boraji.tutorial.spring.ui.base;

public abstract class AbstractBasePresenter {
	public AbstractBasePresenter() {
		MvpInjector.Inject(this);
	}
}
