package com.boraji.tutorial.spring.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.boraji.tutorial.spring.entity.User;
import com.vaadin.ui.Notification;

/**
 * @author imssbora
 * @param <T>
 *
 */
@Repository
@Transactional
public class UserDaoImp<T> implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	private EntityManager em;

	@Override
	public void add(User user) {
		sessionFactory.getCurrentSession().save(user);
	}

	@Override
	public List<User> listUsers() {
		@SuppressWarnings("unchecked")
		TypedQuery<User> query = sessionFactory.getCurrentSession().createQuery("from User");
		return query.getResultList();
	}

	@Override
	public boolean delete(Object value) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(value);
		return true;
	}

	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		TypedQuery<User> query = sessionFactory.getCurrentSession().createQuery("from User");
		return query.getResultList();
	}

//	@Override
//	public List<User> searchByName(String name) {
//		// TODO Auto-generated method stub
//		NativeQuery<User> query = sessionFactory.getCurrentSession()
//		.createNativeQuery("SELECT * from USERS WHERE FIRST_NAME LIKE '%" + name + "%'", User.class);
//		return query.list();
//	}
	
	public void setEntityManager(final EntityManager em) {
		this.em = em;
	}

	@Override
	public List<User> search(String name, String surname, String email) {
		// TODO Auto-generated method stub
		NativeQuery<User> query = sessionFactory.getCurrentSession()
		.createNativeQuery("SELECT * from USERS WHERE FIRST_NAME LIKE '%" + name + "%' and LAST_NAME LIKE '%" + surname + "%' and EMAIL LIKE '%" + email + "%'", User.class);
		return query.list();
	}

	
	@Override
	public void update(User entity) {
		Session session = sessionFactory.getCurrentSession();
		session.update(entity);
		Notification.show(entity.getFirstName()+" updated!");
	}
	
	protected Session getCurrentSession() {
		return sessionFactory.openSession();
	}
}
