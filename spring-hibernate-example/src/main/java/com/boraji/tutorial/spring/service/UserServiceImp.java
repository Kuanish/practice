package com.boraji.tutorial.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boraji.tutorial.spring.dao.UserDao;
import com.boraji.tutorial.spring.entity.User;
import com.vaadin.ui.Notification;

/**
 * @author imssbora
 *
 */
@Service
public class UserServiceImp implements UserService {

   @Autowired
   private UserDao userDao;

   @Transactional
   @Override
   public void add(User user) {
      userDao.add(user);
   }
   

   @Transactional
   @Override
   public void update(User user) {
      userDao.update(user);
	  // Notification.show(user.getFirstName());
   }

   @Transactional(readOnly = true)
   @Override
   public List<User> listUsers() {
      return userDao.listUsers();
   }

   @Transactional
   @Override
   public boolean delete(Object value) {
	// TODO Auto-generated method stub
	   return userDao.delete(value);
   }

   @Override
   public List<User> getAll() {
	   // TODO Auto-generated method stub
	   return userDao.getAll();
   }

   @Override
   public List<User> search(String name, String surname, String email) {
	   // TODO Auto-generated method stub
	   return userDao.search(name,surname,email);
   }
}
