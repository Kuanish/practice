package com.boraji.tutorial.spring.ui.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.boraji.tutorial.spring.entity.User;
import com.boraji.tutorial.spring.service.UserService;
import com.boraji.tutorial.spring.ui.base.AbstractBasePresenter;
import com.boraji.tutorial.spring.ui.user.UserView;
import com.vaadin.ui.Notification;


public class UserPresenter extends AbstractBasePresenter{
	private UserView view;
	
	@Autowired
	private UserService service;
	
	public UserPresenter(UserView view) {
		super();
		this.view = view;	
	}

	public List<User> getUser() {
		// TODO Auto-generated method stub
		
		return service.getAll();
	}

	public List<User> listUsers() {
		// TODO Auto-generated method stub
		return service.listUsers();
	}

	public void delete(User user) {
		// TODO Auto-generated method stub
		service.delete(user);
		//view.setModel(new User());
	}
	
	public void create(User model) {
		service.add(model);
		Notification.show(model.getFirstName()+" created");
	}

	public List<User> search(String name, String surname, String email) {
		// TODO Auto-generated method stub
		return service.search(name,surname,email);
	}

	public void update(User bean3) {
		// TODO Auto-generated method stub
		service.update(bean3);
		//Notification.show("went to presenter");
	}

	
//	public void doSave() {
//		User model = this.view.getModel();
//		User savedUser = service.save(model);
//		view.setModel(savedUser);
//	}
}
